package TP3.model;

import java.util.ArrayList;

public class RegistoAcessos {
    private ArrayList<Acesso> listaAcessos;

    
    public RegistoAcessos() {
        this.listaAcessos = new ArrayList<>();
    }

    public ArrayList<Acesso> getListaAcessos() {
        return listaAcessos;
    }

  public Acesso novoAcesso(){
      Acesso a= new Acesso();
      listaAcessos.add(a);
      return a;
  }  
    
}
