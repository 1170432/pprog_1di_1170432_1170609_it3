package TP3.model;

import java.util.ArrayList;

public class RegistoPerfilDeAutorizacao {

    private ArrayList<PerfilDeAutorizacao> listaPerfis;

    private static final int LIMITE_CHAR_ID = 13;
    private static final int LIMITE_CHAR_DESCRICAO = 150;

    public ArrayList<PerfilDeAutorizacao> getListaPerfis() {
        return listaPerfis;
    }

    public RegistoPerfilDeAutorizacao() {
        this.listaPerfis = new ArrayList<>();
    }

    public PerfilDeAutorizacao novoPerfilDeAutorizacao() {
        PerfilDeAutorizacao p = new PerfilDeAutorizacao();
        listaPerfis.add(p);
        return p;
    }

    public static boolean preValidar(String id, String descricao) {
        return (id.length() < LIMITE_CHAR_ID && !id.isEmpty() && descricao.length() < LIMITE_CHAR_DESCRICAO);
    }
}
