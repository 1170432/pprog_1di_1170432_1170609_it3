package TP3.model;

import java.io.IOException;
import java.util.ArrayList;

public class Transferable {

    private static Empresa empresa;
    private static Equipamento e1;
    private static Equipamento e2;
    private static Equipamento e3;
    private static AreaRestrita a1;
    private static AreaRestrita a2;
    private static Cartao c1;
    private static Cartao c2;
    private static Colaborador col1;
    private static Colaborador col2;
    private static PerfilDeAutorizacao p1;
    private static PerfilDeAutorizacao p2;
    private static PeriodoDeAutorizacao pa1;
    private static PeriodoDeAutorizacao pa2;
    private static ArrayList<Colaborador> listaColaboradors;

    public static Empresa getEmpresa() {
        return empresa;
    }

    public static ArrayList<Colaborador> getListaColaboradores() {
        return listaColaboradors;
    }

    public static PerfilDeAutorizacao getP1() {
        return p1;
    }

    public static void setP1(PerfilDeAutorizacao p1) {
        Transferable.p1 = p1;
    }

    public static void initialize() throws IOException {

        empresa = new Empresa();
        create2AreasRestritas();
        create2CartoesColaboradoresPerfisPeriodos();
        create3Equipamentos();
        LeituraExportacao.lerCartoes();
        LeituraExportacao.lerEquipamentos();
        listaColaboradors = LeituraExportacao.lerColaboradores();

    }

    public static void intializeListaPeriodosAutorizacao() {
    }

    public static void create3Equipamentos() throws IOException {
        e1 = empresa.getRegistoDeEquipamentos().novoEquipamento();
        e1.setEnderecoFisico("01-82-18-FB-XA-12");
        e1.setAreRestrita(a1);
        e1.setMovimento(true);
        e1.setEnderecoLogico("1433:5CD8:90AB:CDEF:FE2C:BA09:8765:5521");
        e1.setDescricao("Equipamento modelo I da marca Xiamoi");
        e2 = empresa.getRegistoDeEquipamentos().novoEquipamento();
        e2.setEnderecoFisico("01-82-12-FB-XA-12");
        e2.setAreRestrita(a1);
        e2.setMovimento(false);
        e2.setEnderecoLogico("1533:5CD8:90AB:CDEF:FEDC:REA09:DF23:5D21");
        e2.setDescricao("Equipamento modelo II da marca Xiamoi");
        e3 = empresa.getRegistoDeEquipamentos().novoEquipamento();
        e3.setEnderecoFisico("IU-81-51-FR-21-67");
        e3.setAreRestrita(a2);
        e3.setMovimento(true);
        e3.setEnderecoLogico("174E:5SD8:90AB:555F:F8DC:BA09:8765:5521");
        e3.setDescricao("Equipamento modelo III da marca Sonia");
        LeituraExportacao.guardarEquipamento(e1);
        LeituraExportacao.guardarEquipamento(e2);
        LeituraExportacao.guardarEquipamento(e3);
    }

    public static void create2AreasRestritas() {
        a1 = new AreaRestrita(10, "Sala de reunioes");
        a2 = new AreaRestrita(8, "Sala secreta");
    }

    public static void create2CartoesColaboradoresPerfisPeriodos() {

        p1 = new PerfilDeAutorizacao();
        p1.setId("Funcionario");
        p1.setDescricao("Perfil destinado aos funcionários do setor 1");
        p1.getListaPeriodosAutorizacao().adicionarPeriodo(e1, Dia.Sabado, 1, 24);
        p2 = new PerfilDeAutorizacao();
        p2.setId("Diretores");
        p2.setDescricao("Perfil destinado aos diretores do setor 1");
        p2.getListaPeriodosAutorizacao().adicionarPeriodo(e1, Dia.Sabado, 1, 24);
        p2.getListaPeriodosAutorizacao().adicionarPeriodo(e2, Dia.Sabado, 1, 24);
        c1 = empresa.getRegistoCartoes().novoCartao(12345, "1/12/2017", "1.2");
        c2 = empresa.getRegistoCartoes().novoCartao(123456, "2/12/2017", "1");
        col1 = new Colaborador(1170432, "Jorge Prado", "Jorge", p1);
        col2 = new Colaborador(1170609, "Austino Andrade", "Austino", p2);
        c1.setAtribuicaoCartao(new AtribuicaoCartao(col1));
        c2.setAtribuicaoCartao(new AtribuicaoCartao(col2));

    }

}
