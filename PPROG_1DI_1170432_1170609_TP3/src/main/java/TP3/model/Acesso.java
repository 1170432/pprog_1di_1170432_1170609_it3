package TP3.model;

public class Acesso {

    private Data data;
    private Tempo hora;
    private boolean movimento;
    private boolean sucesso;
    private String motivo;

    public Acesso(Data data, Tempo hora, boolean movimento, boolean sucesso, String motivo) {
        this.data = data;
        this.hora = hora;
        this.movimento = movimento;
        this.sucesso = sucesso;
        this.motivo = motivo;
    }

    public Acesso() {
    }

    public Data getData() {
        return data;
    }

    public Tempo getHora() {
        return hora;
    }

    public boolean isMovimento() {
        return movimento;
    }

    public boolean isSucesso() {
        return sucesso;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void setHora(Tempo hora) {
        this.hora = hora;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public void setMovimento(boolean movimento) {
        this.movimento = movimento;
    }

    @Override
    public String toString() {
       if(sucesso){
        return "A " + movimentoString() + " feita no dia" + data.toAnoMesDiaString() + " às " + hora.toStringHHMMSS() + " foi " + sucessoString();
       }else{
        return "A " + movimentoString() + " feita no dia" + data.toAnoMesDiaString() + " às " + hora.toStringHHMMSS() + " foi " + sucessoString()+" porque "+motivo;
           
       }
    }

    public String movimentoString() {
        if (movimento) {
            return "entrada";
        } else {
            return "saída";
        }
    }

    public String sucessoString() {
        if (sucesso) {
            return "bem-sucedida";
        } else {
            return "negada";
        }
    }

}
