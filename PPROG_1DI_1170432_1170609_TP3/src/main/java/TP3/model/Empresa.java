package TP3.model;

public class Empresa {

    private RegistoDeEquipamentos r1;
    private RegistoPerfilDeAutorizacao p1;
    private RegistoCartoes c1;

    public Empresa() {
        r1 = new RegistoDeEquipamentos();
        p1 = new RegistoPerfilDeAutorizacao();
        c1 = new RegistoCartoes();
    }

    public RegistoDeEquipamentos getRegistoDeEquipamentos() {
        return r1;
    }

    public RegistoPerfilDeAutorizacao getRegistoPerfilDeAutorizacao() {
        return p1;
    }

    public RegistoCartoes getRegistoCartoes() {
        return c1;
    }

}
