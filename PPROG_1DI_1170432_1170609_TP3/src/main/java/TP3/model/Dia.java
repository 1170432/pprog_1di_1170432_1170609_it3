package TP3.model;

public enum Dia {
    Segunda {
        public String toString() {
            return "Segunda-feira";
        }
    },
    Terca {
        public String toString() {
            return "Terça-feira";
        }

    },
    Quarta {
        public String toString() {
            return "Quarta-feira";
        }

    },
    Quinta {
        public String toString() {
            return "Quinta-feira";
        }

    },
    Sexta {
        public String toString() {
            return "Sexta-feira";
        }

    },
    Sabado {
        public String toString() {
            return "Sábado";
        }

    },
    Domingo {
        public String toString() {
            return "Domingo";
        }

    }
}
