package TP3.model;

public class PerfilDeAutorizacao {

    private String id;
    private String descricao;
    private ListaPeriodosAutorizacao listaPeriodosAutorizacao;

    private static final int LIMITE_CHAR_ID = 13;
    private static final int LIMITE_CHAR_DESCRICAO = 150;

    public PerfilDeAutorizacao() {
        listaPeriodosAutorizacao = new ListaPeriodosAutorizacao();
    }

    public String getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public ListaPeriodosAutorizacao getListaPeriodosAutorizacao() {
        return listaPeriodosAutorizacao;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void createListaPeriodosAutorizacao() {
        listaPeriodosAutorizacao = new ListaPeriodosAutorizacao();
    }

    public boolean validar() {
        return (id.length() < LIMITE_CHAR_ID && !id.isEmpty() && descricao.length() < LIMITE_CHAR_DESCRICAO);
    }

    public boolean isEquipamentoAutorizado(Equipamento equipamento) {
        for (PeriodoDeAutorizacao periodo : listaPeriodosAutorizacao.getListaPeriodosAutorizacao()) {
                boolean b1= periodo.getEquipamento().getEnderecoLogico().equals(equipamento.getEnderecoLogico());
                boolean b2=periodo.getDiaSemana().toString().equalsIgnoreCase(Data.dataAtual().diaDaSemana());
                String a=periodo.getDiaSemana().toString();
                String b=Data.dataAtual().diaDaSemana();
                boolean b3=Tempo.tempoAtual().isMaior(periodo.getTempoInicio());
                boolean b4= periodo.getTempoFim().isMaior(Tempo.tempoAtual());
                boolean b5=periodo.getEquipamento().getAreRestrita().verificarDisponibilidade();
            if (periodo.getEquipamento().getEnderecoLogico().equals(equipamento.getEnderecoLogico()) && periodo.getDiaSemana().toString().equals(Data.dataAtual().diaDaSemana()) && Tempo.tempoAtual().isMaior(periodo.getTempoInicio()) && periodo.getTempoFim().isMaior(Tempo.tempoAtual()) && periodo.getEquipamento().getAreRestrita().verificarDisponibilidade()) {
                return true;
            }
        }
        return false;
    }

}
