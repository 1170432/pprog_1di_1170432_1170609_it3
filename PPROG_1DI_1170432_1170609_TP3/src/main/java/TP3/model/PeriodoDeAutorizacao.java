package TP3.model;

public class PeriodoDeAutorizacao {

    private Dia diaSemana;
    private Tempo tempoInicio;
    private Tempo tempoFim;
    private Equipamento equipamento;

    public PeriodoDeAutorizacao(Equipamento equipamento, Dia diaSemana, int horaInicio, int horaFim) {
        this.equipamento =equipamento;
        tempoInicio=new Tempo(horaInicio);
        tempoFim=new Tempo(horaFim);
        this.diaSemana=diaSemana;
    }
     

    public Dia getDiaSemana() {
        return diaSemana;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public Tempo getTempoInicio() {
        return tempoInicio;
    }

    public Tempo getTempoFim() {
        return tempoFim;
    }
    

    public void setDiaSemana(Dia diaSemana) {
        this.diaSemana = diaSemana;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }
    @Override
    public String toString(){
        return diaSemana.toString()+" das "+tempoInicio.getHoras()+"h ate às "+tempoFim.getHoras()+"h com o equipamento de endereço lógico: "+equipamento.getEnderecoLogico();
    }

    public void validar() {

    }
}
