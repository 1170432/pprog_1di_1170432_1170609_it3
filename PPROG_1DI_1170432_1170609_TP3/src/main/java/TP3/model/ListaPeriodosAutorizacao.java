package TP3.model;

import java.util.ArrayList;

public class ListaPeriodosAutorizacao {
    private ArrayList<PeriodoDeAutorizacao> listaPeriodosAutorizacao;
     
    public ListaPeriodosAutorizacao(){
        listaPeriodosAutorizacao=new ArrayList<>();
    }

    public ArrayList<PeriodoDeAutorizacao> getListaPeriodosAutorizacao() {
        return listaPeriodosAutorizacao;
    }
    
    public void adicionarPeriodo(Equipamento equipamento,Dia dia,int horaInicio,int horaFim){
        PeriodoDeAutorizacao periodo=new PeriodoDeAutorizacao(equipamento,dia, horaInicio,horaFim);
        listaPeriodosAutorizacao.add(periodo);
        
    }
}
