package TP3.model;

public class Equipamento {

    private String enderecoLogico;
    private String enderecoFisico;
    private String fxConf;
    private String descricao;
    private boolean movimento;
    private AreaRestrita areRestrita;

    private static final int LIMITE_CHAR_ENDERECO_LOGICO = 39;
    private static final int LIMITE_CHAR_ENDERECO_FISICO = 17;

    public Equipamento() {
    }

    public Equipamento(Equipamento outroEquipamento) {
        this.enderecoFisico = outroEquipamento.enderecoFisico;
        this.enderecoLogico = outroEquipamento.enderecoLogico;
        this.descricao = outroEquipamento.descricao;
        this.movimento = outroEquipamento.movimento;
        this.fxConf = outroEquipamento.fxConf;
    }

    public Equipamento(String enderecoLogico) {
        this.enderecoLogico = enderecoLogico;
    }

    public String getEnderecoLogico() {
        return enderecoLogico;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getEnderecoFisico() {
        return enderecoFisico;
    }

    public String getFxConf() {
        return fxConf;
    }

    public AreaRestrita getAreRestrita() {
        return areRestrita;
    }

    public boolean getMovimento() {
        return movimento;
    }

    public void setEnderecoFisico(String enderecoFisico) {
        this.enderecoFisico = enderecoFisico;
    }

    public void setEnderecoLogico(String enderecoLogico) {
        this.enderecoLogico = enderecoLogico;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setFxConf(String fxConf) {
        this.fxConf = fxConf;
    }

    public void setMovimento(boolean movimento) {
        this.movimento = movimento;
    }

    public void setAreRestrita(AreaRestrita areRestrita) {
        this.areRestrita = areRestrita;
    }

    @Override
    public String toString() {
        return descricao + "/" + enderecoFisico + "/" + enderecoLogico+"/"+movimentoString();
    }

    public boolean preValidar() {
        return (enderecoLogico.length() == LIMITE_CHAR_ENDERECO_LOGICO && enderecoLogico.matches("[0-9_A-Z_:]+") && enderecoFisico.length() == LIMITE_CHAR_ENDERECO_FISICO) && enderecoFisico.matches("[0-9_A-Z_-]+");
    }
    public String movimentoString(){
        if(movimento){
            return "entrada";
        }else{
            return "saída";
        }
    }

}
