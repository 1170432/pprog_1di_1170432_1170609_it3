package TP3.model;

public class Cartao {

    private int numero;
    private String dtEmissao;
    private String versao;
    private AtribuicaoCartao ac1;

    public Cartao(int numero, String dtEmissao, String versao) {
        this.numero = numero;
        this.dtEmissao = dtEmissao;
        this.versao = versao;
    }

    public Cartao() {
    }

    public Cartao(Cartao outroCartao) {
        this.numero = outroCartao.numero;
        this.dtEmissao = outroCartao.dtEmissao;
        this.versao = outroCartao.versao;
    }

    public int getNumero() {
        return numero;
    }

    public String getDtEmissao() {
        return dtEmissao;
    }

    public String getVersao() {
        return versao;
    }

    public AtribuicaoCartao getAtribuicaoCartao() {
        return ac1;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setDtEmissao(String dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public void setAtribuicaoCartao(AtribuicaoCartao ac1) {
        this.ac1 = ac1;
    }

    public String toString() {
        return  numero + "/" + versao + "/" + dtEmissao;
    }
}
