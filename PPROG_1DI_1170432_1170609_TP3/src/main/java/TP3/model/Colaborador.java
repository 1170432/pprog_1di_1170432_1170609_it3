package TP3.model;

public class Colaborador {

    private int numMecanografico;
    private String nomeCompleto;
    private String nomeAbreviado;
    private PerfilDeAutorizacao perfil;

    public Colaborador(int numMecanografico, String nomeCompleto, String nomeAbreviado, PerfilDeAutorizacao perfil) {
        this.numMecanografico = numMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
        this.perfil = perfil;
    }

    public Colaborador(int numMecanografico, String nomeCompleto, String nomeAbreviado) {
        this.numMecanografico = numMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
    }
    

    public Colaborador() {
    }

    public Colaborador(Colaborador outroColaborador) {
        this.numMecanografico = outroColaborador.numMecanografico;
        this.nomeCompleto = outroColaborador.nomeCompleto;
        this.nomeAbreviado = outroColaborador.nomeAbreviado;
        this.perfil = outroColaborador.perfil;
    }

    public int getNumMecanografico() {
        return numMecanografico;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    public PerfilDeAutorizacao getPerfil() {
        return perfil;
    }

    public void setNumMecanografico(int numMecanografico) {
        this.numMecanografico = numMecanografico;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    public void setPerfil(PerfilDeAutorizacao perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString(){
        return numMecanografico+"/"+nomeAbreviado+"/"+nomeCompleto;
    }
}
