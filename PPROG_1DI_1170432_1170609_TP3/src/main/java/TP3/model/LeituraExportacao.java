package TP3.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class LeituraExportacao {

    private static FileReader frCartao;
    private static FileReader frEquipamento;
    private static FileReader frColaborador;
    private static FileWriter fwCartao;
    private static FileWriter fwEquipamento;
    private static FileWriter fwColaborador;
    private static PrintWriter pwCartao;
    private static PrintWriter pwEquipamento;
    private static PrintWriter pwColaborador;
    private static BufferedReader brCartao;
    private static BufferedReader brColaborador;
    private static BufferedReader brEquipamento;
    private static final String NOME_FICHEIRO_COLABORADOR = "ColaboradorMemory.txt";
    private static final String NOME_FICHEIRO_CARTAO = "CartaoMemory.txt";
    private static final String NOME_FICHEIRO_EQUIPAMENTO = "EquipamentosMemory.txt";

    public LeituraExportacao() {
    }

    public static void limparFicheiros() throws IOException {
        fwCartao = new FileWriter(NOME_FICHEIRO_CARTAO);
        pwCartao = new PrintWriter(fwCartao);
        pwCartao.println("");
        fwEquipamento = new FileWriter(NOME_FICHEIRO_EQUIPAMENTO);
        pwEquipamento = new PrintWriter(fwEquipamento);
        pwEquipamento.println("");
        fwColaborador = new FileWriter(NOME_FICHEIRO_COLABORADOR);
        pwColaborador = new PrintWriter(fwColaborador);
        pwColaborador.println("");
    }

    public static void guardarCartoes(Cartao c) throws IOException {
        fwCartao = new FileWriter(NOME_FICHEIRO_CARTAO, true);
        pwCartao = new PrintWriter(fwCartao);
        pwCartao.println(":");
        pwEquipamento.println(c);
        pwCartao.close();
    }

    public static void guardarEquipamento(Equipamento e) throws IOException {
        fwEquipamento = new FileWriter(NOME_FICHEIRO_EQUIPAMENTO, true);
        pwEquipamento = new PrintWriter(fwEquipamento);
        pwEquipamento.println(":");
        pwEquipamento.println(e);
        pwEquipamento.close();
    }

    public static void guardarColaborador(Colaborador c) throws IOException {
        fwColaborador = new FileWriter(NOME_FICHEIRO_COLABORADOR, true);
        pwColaborador = new PrintWriter(fwColaborador);
        pwColaborador.println(":");
        pwColaborador.println(c);
        pwColaborador.close();
    }

    public static void lerEquipamentos() throws FileNotFoundException, IOException {
        frEquipamento = new FileReader(NOME_FICHEIRO_EQUIPAMENTO);
        brEquipamento = new BufferedReader(frEquipamento);
        String linha = brEquipamento.readLine();
        while (linha != null) {
            if (linha.equals(":")) {
                linha = brEquipamento.readLine();
            }
            String[] temp = linha.split("/");
            Equipamento eqTemp = Transferable.getEmpresa().getRegistoDeEquipamentos().novoEquipamento();
            eqTemp.setDescricao(temp[0]);
            eqTemp.setEnderecoFisico(temp[1]);
            eqTemp.setEnderecoLogico(temp[2]);
            linha = brEquipamento.readLine();
        }
        brEquipamento.close();
    }

    public static void lerCartoes() throws FileNotFoundException, IOException {
        frCartao = new FileReader(NOME_FICHEIRO_CARTAO);
        brCartao = new BufferedReader(frCartao);
        String linha = brCartao.readLine();
        while (linha != null) {
            if (linha.equals(":")) {
                linha = brCartao.readLine();
            }
            String[] temp = linha.split("|");
            Transferable.getEmpresa().getRegistoCartoes().novoCartao(Integer.parseInt(temp[0]), temp[1], temp[2]);
            linha = brCartao.readLine();
        }
        brCartao.close();
    }

    public static ArrayList<Colaborador> lerColaboradores() throws FileNotFoundException, IOException {
        frColaborador = new FileReader(NOME_FICHEIRO_COLABORADOR);
        brColaborador = new BufferedReader(frColaborador);
        String linha = brColaborador.readLine();
        ArrayList<Colaborador> listaColaboradores = new ArrayList<>();
        while (linha != null) {
            if (linha.equals(":")) {
                linha = brCartao.readLine();
            }
            String[] temp = linha.split("|");
            listaColaboradores.add(new Colaborador(Integer.parseInt(temp[0]), temp[2], temp[1]));
            linha = brColaborador.readLine();
        }
        brColaborador.close();
        return listaColaboradores;
    }
}
