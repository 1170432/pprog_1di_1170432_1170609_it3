package TP3.model;

import java.util.ArrayList;

public class RegistoCartoes {
    private ArrayList<Cartao> listaCartoes;

    public RegistoCartoes() {
        this.listaCartoes=new ArrayList<>();
    }

    public ArrayList<Cartao> getListaCartoes() {
        return listaCartoes;
    }
     public Cartao novoCartao(int numero, String dtEmissao, String versao){
         Cartao c=new Cartao(numero,dtEmissao,versao);
         listaCartoes.add(c);
         return c;
     }
     public Cartao novoCartao(){
         Cartao c=new Cartao();
         listaCartoes.add(c);
         return c;
     }
     public Cartao procurarCartao(int id){
         for (Cartao c: listaCartoes){
             if(c.getNumero()==id){
                 return c;
             }
         }
         return null;
     }
    
}
