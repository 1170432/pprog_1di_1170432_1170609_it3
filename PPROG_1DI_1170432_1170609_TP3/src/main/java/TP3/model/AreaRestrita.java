package TP3.model;

public class AreaRestrita {

    private String nome;
    private int lotacao;
    private int lotacaoMaxima;
    private RegistoAcessos registoAcessos;

    public AreaRestrita(int lotacaoMaxima, String nome) {
        this.lotacaoMaxima = lotacaoMaxima;
        this.nome = nome;
        lotacaoMaxima = 0;
        registoAcessos= new RegistoAcessos();
    }

    public int getLotacao() {
        return lotacao;
    }

    public int getLotacaoMaxima() {
        return lotacaoMaxima;
    }

    public String getNome() {
        return nome;
    }

    public RegistoAcessos getRegistoAcessos() {
        return registoAcessos;
    }

    public void setLotacaoMaxima(int lotacaoMaxima) {
        this.lotacaoMaxima = lotacaoMaxima;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setRegistoAcessos(RegistoAcessos registoAcessos) {
        this.registoAcessos = registoAcessos;
    }

    public boolean verificarDisponibilidade() {
        return (lotacao != lotacaoMaxima);
    }
    public void registarAcesso(boolean  movimento,boolean acessoNaoAutorizado){
        if(movimento){
            entrada(acessoNaoAutorizado);
        }else{
            saida(acessoNaoAutorizado);
        }
    }
    public void entrada(boolean acessoNaoAutorizado){
        Acesso a=registoAcessos.novoAcesso();
        a.setData(Data.dataAtual());
        a.setHora(Tempo.tempoAtual());
        a.setMovimento(true);
        if(!verificarDisponibilidade()){
            a.setMotivo("área lotada");
        }
        if(acessoNaoAutorizado){
            a.setMotivo("cartao não garante acesso à área");
        }
        lotacao++;
    }
    public void saida(boolean acessoNaoAutorizado){
        Acesso a=registoAcessos.novoAcesso();
        a.setData(Data.dataAtual());
        a.setHora(Tempo.tempoAtual());
        a.setMovimento(false);
        if(acessoNaoAutorizado){
            a.setMotivo("cartao não garante acesso à área");
        }
        lotacao--;
    }
    @Override
    public String toString(){
        return nome+"|"+lotacaoMaxima;
    }
}
