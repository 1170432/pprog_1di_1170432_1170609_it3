package TP3.model;

import java.util.ArrayList;

public class RegistoDeEquipamentos {

    private ArrayList<Equipamento> listaEquipamentos;

    private static final int LIMITE_CHAR_ENDERECO_LOGICO = 39;
    private static final int LIMITE_CHAR_ENDERECO_FISICO = 17;

    public RegistoDeEquipamentos() {
        this.listaEquipamentos = new ArrayList<>();
    }

    public ArrayList<Equipamento> getListaEquipamentos() {
        return listaEquipamentos;
    }

    public Equipamento novoEquipamento() {
        Equipamento e = new Equipamento();
        listaEquipamentos.add(e);
        return e;
    }

    public static boolean preValidar(String enderecoLogico, String enderecoFisico) {
        return (enderecoLogico.length() == LIMITE_CHAR_ENDERECO_LOGICO && enderecoLogico.matches("[0-9_A-Z_:]+") && enderecoFisico.length() == LIMITE_CHAR_ENDERECO_FISICO) && enderecoFisico.matches("[0-9_A-Z_-]+");
    }
}
