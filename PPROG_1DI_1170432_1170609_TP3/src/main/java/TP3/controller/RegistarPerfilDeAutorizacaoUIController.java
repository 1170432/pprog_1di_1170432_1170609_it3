package TP3.controller;

import TP3.model.*;
import TP3.model.RegistoPerfilDeAutorizacao;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class RegistarPerfilDeAutorizacaoUIController implements Initializable {

    @FXML
    private TextField textId;
    @FXML
    private TextField textDesc;
    @FXML
    private Button bttnValidarGuardar;
    @FXML
    private Button bttnRetrodecerMenu;
    @FXML
    private AnchorPane Uc3Pane1;
    private RegistarPerfilDeAutorizacao2Controller rpController;
    private RegistarPerfilDeAutorizacao3Controller rpController2;
    private FXMLLoader loader;
    private FXMLLoader loader2;

    private PerfilDeAutorizacao p;
    private RegistoPerfilDeAutorizacao rp;
    private boolean next;
    @FXML
    private Button bttnSeguinte;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rp = Transferable.getEmpresa().getRegistoPerfilDeAutorizacao();
        p = rp.novoPerfilDeAutorizacao();
        next = false;
        bttnSeguinte.setOpacity(.4);
        loader = new FXMLLoader(getClass().getResource("/fxml//RegistarPerfilDeAutorizacao2.fxml"));
        loader2 = new FXMLLoader(getClass().getResource("/fxml//RegistarPerfilDeAutorizacao3.fxml"));
        rpController = loader.getController();
        rpController = new RegistarPerfilDeAutorizacao2Controller();
        rpController2 = loader2.getController();
        rpController2 = new RegistarPerfilDeAutorizacao3Controller();
    }

    @FXML
    private void clickBttnValidarEGuardar(MouseEvent event) {
        if (RegistoPerfilDeAutorizacao.preValidar(textId.getText().trim(), textDesc.getText().trim())) {
            p.setId(textId.getText().trim());
            p.setDescricao(textDesc.getText().trim());
            Transferable.setP1(p);
            Alert sucesso = new Alert(Alert.AlertType.INFORMATION);
            sucesso.setTitle("Dados Guardados");
            sucesso.setHeaderText("Sucesso!");
            sucesso.setContentText("Os dados introduzidos foram guardados.\n\n Caso não pretenda fazer alguma alteração proceda para a página seguinte");
            sucesso.show();
            next = true;
            bttnSeguinte.setOpacity(1);

        } else {
            Alert alerta = new Alert(Alert.AlertType.ERROR);

            alerta.setTitle("Dados Inválidos");
            alerta.setHeaderText("Oops deu erro!");
            alerta.setContentText("Os dados introduzidos não se encontram de acordo com os parâmetros.\n\n Tente Novamente!");
            alerta.show();
        }

    }

    @FXML
    private void clickBttnRetrocederMenu(MouseEvent event) throws IOException {
        AnchorPane menuPane = FXMLLoader.load(getClass().getResource("/fxml//MenuPrincipal.fxml"));
        Uc3Pane1.getChildren().setAll(menuPane);
    }

    @FXML
    private void clickBttnSeguinte(MouseEvent event) throws IOException {
        if (next) {
            AnchorPane nextPane = FXMLLoader.load(getClass().getResource("/fxml/RegistarPerfilDeAutorizacao2.fxml"));
            Uc3Pane1.getChildren().setAll(nextPane);
        }

    }

    public PerfilDeAutorizacao getP() {
        return p;
    }

    public RegistoPerfilDeAutorizacao getRp() {
        return rp;
    }

}
