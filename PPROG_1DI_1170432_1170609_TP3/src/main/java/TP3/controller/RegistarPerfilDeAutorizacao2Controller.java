/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP3.controller;

import TP3.model.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author Wz
 */
public class RegistarPerfilDeAutorizacao2Controller implements Initializable {

    @FXML
    private ChoiceBox<String> ChoiceBxListaEquipamentos;
    @FXML
    private Button bttnSeguinte;
    @FXML
    private TextField horaInicio;
    @FXML
    private TextField horaFim;
    @FXML
    private ChoiceBox<Dia> ChoiceBxListaDias;
    @FXML
    private Button bttnRetroceder;
    @FXML
    private AnchorPane Uc3Pane2;
    @FXML
    private Button bttnAdicionarEquipamentoPeriodo;

    private ArrayList<String> listaEquipamentosString;
    private boolean next;
    private boolean createLista;
    private PerfilDeAutorizacao p;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        preencherChoiceBoxEquipamento(Transferable.getEmpresa().getRegistoDeEquipamentos().getListaEquipamentos());
        ChoiceBxListaDias.setItems(FXCollections.observableArrayList(Dia.values()));
        next = false;
        createLista = false;
        bttnSeguinte.setOpacity(.4);
        p=Transferable.getP1();
    }

    @FXML
    private void clickBttnSeguinte(MouseEvent event) throws IOException {
        if (next) {
            AnchorPane nextPane = FXMLLoader.load(getClass().getResource("/fxml/RegistarPerfilDeAutorizacao3.fxml"));
            Uc3Pane2.getChildren().setAll(nextPane);
        }
    }

    @FXML
    private void clickBttnRetroceder(MouseEvent event) throws IOException {
        AnchorPane menuPane = FXMLLoader.load(getClass().getResource("/fxml/RegistarPerfilDeAutorizacao1.fxml"));
        Uc3Pane2.getChildren().setAll(menuPane);
    }

    @FXML
    private void clickBttnAdicionarEquipamentoPeriodo(MouseEvent event) {
        int indiceEquipamentoEscolhido = listaEquipamentosString.indexOf(ChoiceBxListaEquipamentos.getSelectionModel().getSelectedItem());
        if (preValidarHoras(horaInicio, horaFim)) {
            if (!createLista) {
                p.createListaPeriodosAutorizacao();
                createLista=true;
            }
            p.getListaPeriodosAutorizacao().adicionarPeriodo(Transferable.getEmpresa().getRegistoDeEquipamentos().getListaEquipamentos().get(indiceEquipamentoEscolhido), ChoiceBxListaDias.getSelectionModel().getSelectedItem(), Integer.parseInt(horaInicio.getText().trim()), Integer.parseInt(horaFim.getText().trim()));
            Alert sucesso = new Alert(Alert.AlertType.INFORMATION);
            sucesso.setTitle("Dados Guardados");
            sucesso.setHeaderText("Sucesso!");
            sucesso.setContentText("O equipamento autorizado e respetivo periodo de autorização foram introduzidos.\n\n Caso não pretenda acrescentar mais periodos/equipametos proceda para a página seguinte");
            sucesso.show();
            next = true;
            bttnSeguinte.setOpacity(1);
        } else {
            Alert alerta = new Alert(Alert.AlertType.ERROR);

            alerta.setTitle("Dados Inválidos");
            alerta.setHeaderText("Oops deu erro!");
            alerta.setContentText("Os dados introduzidos não se encontram de acordo com os parâmetros.\n\n Verifique que as horas foram corretamente introduzidas e um equipamento e dia foram selecionados!");
            alerta.show();
        }

    }

    public void preencherChoiceBoxEquipamento(ArrayList<Equipamento> listaEquipamentos) {
        listaEquipamentosString = new ArrayList<>();
        for (Equipamento equipamento : listaEquipamentos) {
            listaEquipamentosString.add(equipamento.toString());

        }
        ChoiceBxListaEquipamentos.setItems(FXCollections.observableArrayList(listaEquipamentosString));
    }

    public boolean preValidarHoras(TextField hora, TextField horaFim) {
        String horaText = hora.getText();
        String horaFimText = horaFim.getText();
        return (horaText.matches("[0-9]+") && horaFimText.matches("[0-9]+") && Integer.parseInt(horaText) >= 0 && Integer.parseInt(horaText) <= 24 && Integer.parseInt(horaFimText) >= 0 && Integer.parseInt(horaFimText) <= 24 && Integer.parseInt(horaText) <= Integer.parseInt(horaFimText) && !ChoiceBxListaDias.getSelectionModel().isEmpty() && !ChoiceBxListaEquipamentos.getSelectionModel().isEmpty() );
    }

    public void setPerfilFromController(PerfilDeAutorizacao p) {
        this.p = p;
    }
}
