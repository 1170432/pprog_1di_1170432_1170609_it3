/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP3.controller;

import TP3.model.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Wz
 */
public class MenuPrincipalController implements Initializable {

    @FXML
    private Button bttnUc3;
    @FXML
    private Button bttnUc6;
    @FXML
    private Button bttnAcercaDe;
    @FXML
    private Label textoCentral;
    @FXML
    private AnchorPane menuPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Transferable.initialize();
            LeituraExportacao.limparFicheiros();
        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void clickBttnUc3(MouseEvent event) throws IOException {
        AnchorPane Uc3Pane1= FXMLLoader.load(getClass().getResource("/fxml/RegistarPerfilDeAutorizacao1.fxml"));
        menuPane.getChildren().setAll(Uc3Pane1);
               
    }

    @FXML
    private void cliclBttnUc6(MouseEvent event) throws IOException {
        AnchorPane Uc6Pane= FXMLLoader.load(getClass().getResource("/fxml/AcederAreaRestrita.fxml"));
        menuPane.getChildren().setAll(Uc6Pane);
    }

    @FXML
    private void clickBttnAcercaDe(MouseEvent event) throws IOException {
         AnchorPane AcercaDePane= FXMLLoader.load(getClass().getResource("/fxml/AcercaDe.fxml"));
        menuPane.getChildren().setAll(AcercaDePane);
    }
    
}