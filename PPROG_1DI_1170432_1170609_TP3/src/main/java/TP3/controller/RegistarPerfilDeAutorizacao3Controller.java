/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP3.controller;
import TP3.model.*;
import TP3.model.PerfilDeAutorizacao;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Wz
 */
public class RegistarPerfilDeAutorizacao3Controller implements Initializable {

    @FXML
    private ListView<String> listEquipamentosAutorizados;
    @FXML
    private ListView<String> listPeriodosDeAutorizacao;

     private PerfilDeAutorizacao p;
    @FXML
    private Button bttnConfirmarPerfil;
    @FXML
    private Label txtId;
    @FXML
    private Label txtDescricao;
    
    private ArrayList<String> listaEquipamentosString;
    @FXML
    private AnchorPane Uc3Pane3;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        p=Transferable.getP1();
        txtId.setText("Id: "+p.getId());
        txtDescricao.setText("Desc: "+p.getDescricao());
        preencherChoiceBoxEquipamento(p.getListaPeriodosAutorizacao().getListaPeriodosAutorizacao());
        preencherChoiceBoxPeriodos(p.getListaPeriodosAutorizacao().getListaPeriodosAutorizacao());
    }    
     public void setPerfilFromController(PerfilDeAutorizacao p) {
        this.p = p;
    }

    @FXML
    private void cliclBttnConfirmarPerfil(MouseEvent event) throws IOException {
         AnchorPane menuPane = FXMLLoader.load(getClass().getResource("/fxml//MenuPrincipal.fxml"));
        Uc3Pane3.getChildren().setAll(menuPane);
    }
    public void preencherChoiceBoxEquipamento(ArrayList<PeriodoDeAutorizacao> listaPeriodo) {
        listaEquipamentosString = new ArrayList<>();
        for (PeriodoDeAutorizacao periodo : listaPeriodo) {
            listaEquipamentosString.add(periodo.getEquipamento().toString());

        }
        listEquipamentosAutorizados.setItems(FXCollections.observableArrayList(listaEquipamentosString));
    }
    public void preencherChoiceBoxPeriodos(ArrayList<PeriodoDeAutorizacao> listaPeriodos) {
        listaEquipamentosString = new ArrayList<>();
        for (PeriodoDeAutorizacao periodo : listaPeriodos) {
            listaEquipamentosString.add(periodo.toString());

        }
        listPeriodosDeAutorizacao.setItems(FXCollections.observableArrayList(listaEquipamentosString));
    }
    
}
