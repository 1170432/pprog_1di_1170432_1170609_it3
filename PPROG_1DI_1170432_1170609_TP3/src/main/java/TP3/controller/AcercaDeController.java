/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP3.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Wz
 */
public class AcercaDeController implements Initializable {

    @FXML
    private AnchorPane AcercaDePane;
    @FXML
    private Button bttnRetroceder;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clickBttnRetrodecer(MouseEvent event) throws IOException {
        AnchorPane Uc3Pane1= FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));
        AcercaDePane.getChildren().setAll(Uc3Pane1);
        
    }
    
}
