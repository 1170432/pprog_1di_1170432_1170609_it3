package TP3.controller;

import TP3.model.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

public class AcederAreaRestritaController implements Initializable {

    @FXML
    private TextField txtID;
    @FXML
    private Button bttnAcederAreaRestrita;
    @FXML
    private Label txtAcesso;
    @FXML
    private Button bttnRetroceder;
    @FXML
    private ChoiceBox<String> choiceBoxListEquipamentos;

    private ArrayList<String> listaEquipamentosString;
    @FXML
    private AnchorPane Uc6Pane;
    private static Color verde = new Color(.4, 0.8, .4, 1);
    private static Color vermelho = new Color(.8, 0.4, .4, 1);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preencherChoiceBoxEquipamento(Transferable.getEmpresa().getRegistoDeEquipamentos().getListaEquipamentos());
    }

    @FXML
    private void clickBttnAcederAreaRestrita(MouseEvent event) {
        if (preValidarId()) {
            Cartao c = Transferable.getEmpresa().getRegistoCartoes().procurarCartao(Integer.parseInt(txtID.getText()));
            if (c != null) {
                int indiceEquipamentoEscolhido = listaEquipamentosString.indexOf(choiceBoxListEquipamentos.getSelectionModel().getSelectedItem());
                ArrayList<Equipamento> listaaa=Transferable.getEmpresa().getRegistoDeEquipamentos().getListaEquipamentos();
                Equipamento e=Transferable.getEmpresa().getRegistoDeEquipamentos().getListaEquipamentos().get(indiceEquipamentoEscolhido);
               if(c.getAtribuicaoCartao().getColaborador().getPerfil().isEquipamentoAutorizado(e)){
                   e.getAreRestrita().registarAcesso(e.getMovimento(), false);
                   txtAcesso.setText("Acesso válido, pode entrar!");
                   txtAcesso.setTextFill(verde);
               }else{
                   e.getAreRestrita().registarAcesso(e.getMovimento(), true);
                   txtAcesso.setText("Acesso inválido, não tem acesso a esta área!");
                   txtAcesso.setTextFill(vermelho);
               }
            } else {
                Alert alerta = new Alert(Alert.AlertType.ERROR);
                alerta.setTitle("Dados Inválidos");
                alerta.setHeaderText("Oops deu erro!");
                alerta.setContentText("O número de indentificação introduzido não se encontram na nossa base de dados.\n\n Tente de novo!");
                alerta.show();

            }
        } else {
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Dados Inválidos");
            alerta.setHeaderText("Oops deu erro!");
            alerta.setContentText("O número de indentificação introduzido não se encontram de acordo com os parâmetros.\n\n Tente de novo!");
            alerta.show();
        }
    }

    @FXML
    private void clickBttnRetroceder(MouseEvent event) throws IOException {
        AnchorPane menuPane = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));
        Uc6Pane.getChildren().setAll(menuPane);
    }

    public void preencherChoiceBoxEquipamento(ArrayList<Equipamento> listaEquipamentos) {
        listaEquipamentosString = new ArrayList<>();
        for (Equipamento equipamento : listaEquipamentos) {
            listaEquipamentosString.add(equipamento.toString());

        }
        choiceBoxListEquipamentos.setItems(FXCollections.observableArrayList(listaEquipamentosString));
    }

    public boolean preValidarId() {
        return !txtID.getText().isEmpty() && txtID.getText().matches("[0-9]+");
    }

    public void alertaIdNaoEncontrado() {

    }

    public void alertaTxtIdInvalido() {
    }

}
